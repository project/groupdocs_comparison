GroupDocs ComparisonWhether you need to compare two word documents, excel spreadsheets or PDF files – GroupDocs Comparison is here to help you. Use it to compare documents online, from within your web browser


-- INSTALLATION --

1. Unpack the groupdocs_comparison folder and contents in the appropriate modules
   directory of your Drupal installation. This is probably sites/all/modules/
2. Enable Embedded Groupdocs Comparison in the admin modules section.
3. Click on "manage fields" under Home » Administration » Structure and
   add new field with type "GroupDocs comparison".
4. Go edit or add new node of type that you added new field to. Enter redline Guid to the text input.

   Note: Current version of module support creation of only one field!
